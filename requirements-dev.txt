-r requirements.txt

pytest<4.6.0
pytest-mock
pytest-timeout
pytest-asyncio
pytest-spec
coverage
invoke
pylint
tavern

sphinx
sphinx_rtd_theme
autodoc

docker-compose
twine
